import java.util.Scanner;

public class Activitat3 {

    public static void main(String[] args) {

        Scanner tec = new Scanner (System.in);

        int[] numeros = new int[20];

        for (int i = 0; i <20 ; i++) {
            System.out.println();

            System.out.println("Introduce numero en el Array:");

            numeros[i]= tec.nextInt();
        }
        System.out.println("Los numeros introducidos son: ");
        for (int j = 0; j <20; j++){

            if(j%4==0){
                System.out.println();
            }
            System.out.print(numeros[j]);

        }
    }


}
