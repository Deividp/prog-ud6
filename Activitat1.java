import java.util.Scanner;

public class Activitat1 {

    public static void main(String[] args) {

        Scanner tec = new Scanner (System.in);

        int[] numeros = new int[10];

        for (int i = 0; i <10 ; i++) {
            System.out.println();

            System.out.println("Introduce numero en el Array:");

            numeros[i]= tec.nextInt();
        }
        System.out.println("Los numeros introducidos son: ");
        for (int j = 0; j <10; j++){
            System.out.println(numeros[j] );
        }
    }

}
