package UD6EJER5;

public class MyArray {
    private int[] array;

    public int[] crearArray() {

        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        this.array = array;
        return array;

    }

    public void visualitzarArray() {
        System.out.println();
        for (int i = 0; i < this.array.length -1; i++) {
            System.out.println(this.array[i] + " ");
        }

    }

    public void visualitzarContParell() {
        System.out.println();
        for (int i = 0; i < this.array.length -1; i++) {

            if (this.array[i] % 2 == 0) {
                System.out.println("Posicion " + i + " esta el numero " + this.array[i] + ".");
            }

        }
    }
            public void visualitzarContSenar() {
                System.out.println();
            for (int i = 0; i < this.array.length -1; i++) {

                if (this.array[i] % 2 != 0) {
                    System.out.println("Posicion " + i + " esta el numero " + this.array[i] + ".");
                }

            }

        }

    }
