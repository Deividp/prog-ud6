public class Activitat7 {


    public static void main(String[] args) {

        int [][] arrayDoble = new int[5][7];

        for (int i = 0; i < arrayDoble.length; i++){
            for (int j = 0; j < arrayDoble[i].length; j++){

                arrayDoble[i][j]= (int) (Math.random()*10);
            }
        }
        System.out.println("La matriz completa: ");
        for (int i = 0; i < 5; i++){
            System.out.println();
            System.out.println();
            for (int j = 0; j < 7; j++){
                System.out.print(arrayDoble[i][j]+" ");
            }
        }

        System.out.println("\nLa quinta fila de la matriz: ");
        for (int i = 0; i < 5; i++){
            System.out.println();

            for (int j = 0; j < 7; j++){

                if(i==4){
                    System.out.print(arrayDoble[i][j]+" ");
                }else{
                    System.out.print(" ");
                }

            }

            int aux = arrayDoble[3][2];
            arrayDoble[3][2] = arrayDoble[4][2];
            arrayDoble[4][2] = aux;

        }
        System.out.println("\nLa matriz completa: ");
        for (int i = 0; i < 5; i++){
            System.out.println();
            System.out.println();
            for (int j = 0; j < 7; j++){
                System.out.print(arrayDoble[i][j]+" ");
            }
        }

        int [] aux = new int [5];

        for (int i = 0; i < 5; i++){
            aux[i] = arrayDoble[i][0];
        }
        for (int i = 0; i < 5; i++){
            arrayDoble[i][0] = arrayDoble[i][3];
        }
        for (int i = 0; i < 5; i++){
            arrayDoble[i][3] = aux[i];

        }
        System.out.println("\nLa matriz completa: ");
        for (int i = 0; i < 5; i++){
            System.out.println();
            System.out.println();
            for (int j = 0; j < 7; j++){
                System.out.print(arrayDoble[i][j]+" ");
            }
        }
    }
}
