import java.util.Scanner;

public class Activitat2 {

    public static void main(String[] args) {


        Scanner tec = new Scanner (System.in);

        char[] numeros = new char[10];

        for (int i = 0; i <10 ; i++) {
            System.out.println();

            System.out.println("Introduce numero en el Array:");

            numeros[i]= tec.next().charAt(0);
        }
        System.out.println("Los caracteres introducidos en posicion par son: ");
        for (int j = 0; j <10; j++){

            if (j%2==0){
                System.out.println(numeros[j]);
            }
        }
        System.out.println("Los caracteres introducidos en posicion inpar son: ");
        for (int i = 0; i <10 ; i++) {
            if (i %2 !=0){
                System.out.println(numeros[i]);
            }
        }
    }
}
